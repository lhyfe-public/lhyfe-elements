import { NgModule } from '@angular/core';
import { ButtonComponent } from './components/button/button.component';
import { PrimaryButton } from './components/button/primary-button.component';
import { SecondaryButton } from './components/button/secondary-button.component';
import { IconButton } from './components/button/icon-button.component';

@NgModule({
  declarations: [],
  imports: [
    ButtonComponent,
    PrimaryButton,
    SecondaryButton,
    IconButton
  ],
  exports: [
    ButtonComponent,
    PrimaryButton,
    SecondaryButton,
    IconButton
  ]
})
export class UiModule { }
