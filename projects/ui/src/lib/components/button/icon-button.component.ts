import { CommonModule } from "@angular/common";
import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'icon-button',
    standalone: true,
    imports: [CommonModule],
    template: ` <div class="button-wrapper">
        <button
            type="button"
            (click)="onClick.emit($event)"
            [ngClass]="classes"
            [ngStyle]="{ 'background-color': backgroundColor }"
        >
            <div class="button--icon-container">
                <img [src]="icon" [ngClass]="iconClasses">
                <span>{{ label }}</span>
            </div>
        </button>
    </div>`,
    styleUrls: ['./button.css'],
})
export class IconButton {
    @Input()
    backgroundColor?: string;

    @Input()
    size: 'small' | 'medium' | 'large' = 'medium';

    /**
     * Button contents
     * 
     * @required
     * 
     */
    @Input()
    label = 'Button';

    @Input()
    icon = '/assets/icons/profile.png';

    @Output()
    onClick = new EventEmitter<Event>();

    public get classes(): string[] {
        return ['button', 'button--icon', `button--icon-${this.size}`];
    }

    public get iconClasses(): string[] {
        return [`icon-${this.size}`];
    }
}