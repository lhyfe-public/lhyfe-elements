import { CommonModule } from "@angular/common";
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'primary-button',
    standalone: true,
    imports: [CommonModule],
    template: ` <div class="button-wrapper">
        <button
            type="button"
            (click)="onClick.emit($event)"
            [ngClass]="classes"
            [ngStyle]="{ 'background-color': backgroundColor }"
        >
            {{ label }}
        </button>
    </div>`,
    styleUrls: ['./button.css'],
})
export class PrimaryButton {
    @Input()
    backgroundColor?: string;

    @Input()
    size: 'small' | 'medium' | 'large' = 'medium';

    /**
     *  Button contents
     * 
     * @required
     * 
     *  */ 
    @Input()
    label = 'Button';

    @Output()
    onClick = new EventEmitter<Event>();

    public get classes(): string[] {
        return ['button', 'button--primary', `button--${this.size}`];
    }
}