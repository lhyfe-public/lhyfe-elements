import type { Meta, StoryObj } from "@storybook/angular";

import { SecondaryButton } from "../lib/components/button/secondary-button.component";

const meta: Meta<SecondaryButton> = {
    title: 'Example/SecondaryButton',
    component: SecondaryButton,
    tags: ['autodocs'],
    render: (args: SecondaryButton) => ({
        props: {
            backgroundColor: null,
            ...args,
        },
    }),
    argTypes: {
        backgroundColor: {
            control: 'color',
        },
    },
};

export default meta;
type Story = StoryObj<SecondaryButton>;

export const small: Story = {
    args: {
        label: 'Button',
        size: 'small'
    },
};

export const medium: Story = {
    args: {
        label: 'Button',
        size: 'medium'
    },
};

export const Large: Story = {
    args: {
        label: 'Button',
        size: 'large'
    },
};