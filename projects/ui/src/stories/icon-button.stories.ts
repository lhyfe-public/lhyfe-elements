import type { Meta, StoryObj } from "@storybook/angular";

import { IconButton } from "../lib/components/button/icon-button.component";

const meta: Meta<IconButton> = {
    title: 'Example/IconButton',
    component: IconButton,
    tags: ['autodocs'],
    render: (args: IconButton) => ({
        props: {
            backgroundColor: null,
            ...args,
        },
    }),
    argTypes: {
        backgroundColor: {
            control: 'color',
        },
    },
};

export default meta;
type Story = StoryObj<IconButton>;

export const small: Story = {
    args: {
        label: 'Profile',
        size: 'small'
    },
};

export const medium: Story = {
    args: {
        label: 'Profile',
        size: 'medium'
    },
};

export const Large: Story = {
    args: {
        label: 'Profile',
        size: 'large'
    },
};