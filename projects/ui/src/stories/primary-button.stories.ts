import type { Meta, StoryObj } from '@storybook/angular';

import { PrimaryButton } from '../lib/components/button/primary-button.component';

const meta: Meta<PrimaryButton> = {
    title: 'Example/PrimaryButton',
    component: PrimaryButton,
    tags: ['autodocs'],
    render: (args: PrimaryButton) => ({
        props: {
            backgroundColor: null,
            ...args,
        },
    }),
    argTypes: {
        backgroundColor: {
            control: 'color',
        },
    },
};

export default meta;
type Story = StoryObj<PrimaryButton>;

export const small: Story = {
    args: {
        label: 'Button',
        size: 'small'
    },
};

export const medium: Story = {
    args: {
        label: 'Button',
        size: 'medium',
    },
};

export const Large: Story = {
    args: {
        label: 'Button',
        size: 'large',
    },
};